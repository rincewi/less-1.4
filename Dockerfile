FROM debian:9 AS builder

ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.1

RUN apt update && apt install -y wget build-essential libpcre3-dev zlib1g zlib1g-dev libssl-dev

RUN mkdir /nginx_build
WORKDIR /nginx_build
RUN wget https://nginx.org/download/nginx-1.17.8.tar.gz \
  && wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz \
  && wget https://github.com/openresty/luajit2/archive/v2.1-20200102.tar.gz \
  && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.15.tar.gz \
  && wget https://github.com/openresty/lua-resty-core/archive/v0.1.17.tar.gz \
  && tar -xzvf v0.1.17.tar.gz \
  && tar -xzvf nginx-1.17.8.tar.gz \
  && tar -xzvf v0.10.15.tar.gz \
  && tar -xzvf v0.3.1.tar.gz \
  && tar -xzvf v2.1-20200102.tar.gz

RUN cd luajit2-2.1-20200102 \
    && make \
    && make install

RUN cd nginx-1.17.8 \
    && ./configure --with-ld-opt="-Wl,-rpath,/usr/local/lib" \
       --add-module=/nginx_build/ngx_devel_kit-0.3.1 \
       --add-module=/nginx_build/lua-nginx-module-0.10.15 \
    && make -j2 \
    && make install

RUN cd lua-resty-core-0.1.17 \
    && make \
    && make install 

FROM debian:buster-slim
RUN apt update && apt install -y gcc make
COPY --from=builder /nginx_build/luajit2-2.1-20200102 /home/luajit2-2.1-20200102
COPY --from=builder /usr/local/lib/lua /usr/local/lib/lua
RUN cd /home/luajit2-2.1-20200102 \
    && make \
    && make install
RUN rm -rf /home/luajit2-2.1-20200102
WORKDIR /usr/local/nginx/sbin
RUN mkdir ../logs ../conf \
    && touch ../logs/error.log 
COPY --from=builder /usr/local/nginx/sbin/nginx .
COPY --from=builder /usr/local/nginx/conf/mime.types ../conf
RUN chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
